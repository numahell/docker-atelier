# Avant Propos / Preface

(English version below)

## Français

Si vous arrivez ici par hasard, un petit historique s'impose.
Il était une fois, une petite association qui vivait dans les bois français et qui voulait favoriser la décentralisation des services. Cette association, appelée Framasoft, se rendit bien compte que pour pouvoir proposer des services alternatifs aux grands méchants de cette histoire, la famille des géants GAFAM, il fallait à la fois proposer des services fonctionnels, mais de surcroit les multiplier localement. De cette manière, les petits poucets locaux, ou les associations de petits poucet, pourraient frapper physiquement à la porte d'un Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires, un CHATONS.

Trêve d'histoire. Étant bien entendu que les CHATONS vont sauver le web, ce projet vise la facilitation de la création d'un CHATONS, autrement dit le déploiement automatique d'un hébergeur de services s'inscrivant dans le dispositif de dissémination de Framasoft "CHATONS" (https://framablog.org/tag/chatons/).
Le but étant de fournir un unique script d'installation tournant sur une fresh install de Debian pour obtenir un CHATONS fonctionnel.

## English

Hey! Welcome to our English speaking friends!
If you stumble upon this project, you'll need a little story.
Once upon a time, there was a little organization, called Framasoft, who used to live in the French woods. She wanted to decentralize hosting and other services trusted by the big bad guys of this story, the GAFAM family. In order to do so, they had to offer not only functional services, but to develop a network of small groups which will provide hosting services locally. The local families and organizations could then ask one of the locals Keen Internet Talented Teams Engaged in Network Services, a KITTENS, to provide fair services.

Joking aside, as KITTENS will save the world-wide web, this project aims at easing the deployment of a KITTENS, CHATONS in French (https://framablog.org/tag/chatons/). In other words, we aim at an automatic deployment of a fair-hosting service following chatons.org rules, in a single script launchable on a fresh install of debian.

# Installation

## Français

### Pré-requis

Pour pouvoir installer un CHATONS, vous devez disposer :
- d'une machine/VM avec une nouvelle installation de debian
- du domaine et des sous-domaines (cf. services présent dans `docker-compose.yml`) pointant sur l'ip publique de cette machine

### Installation

Pour installer et configurer un CHATONS, suivez install.md en tant que root.

## English

## Lisez-moi

[Pad](https://mypads.framapad.org/mypads/?/mypads/group/altermediatic-toulouse-deatm79d/pad/view/docker-atelier-acqwh7km)

# Procédure d’installation

[install instructions](install.md)
